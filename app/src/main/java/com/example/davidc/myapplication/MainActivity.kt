package com.example.davidc.myapplication


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {
    internal lateinit var numeroTextView: TextView
    internal lateinit var puntajeTextView: TextView
    internal lateinit var startButton: Button
    internal lateinit var stopButton: Button

    internal var gameStarted = false
    internal var score = 0
    internal var n =0
    internal var tiempo=0
    internal lateinit var countDownTimer: CountDownTimer
    internal val initialCountDown = 10000L
    internal var countDownInterval = 1000L
    internal var timeLeft = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        numeroTextView = findViewById(R.id.idNumero)
        puntajeTextView = findViewById(R.id.idPuntaje)
        startButton = findViewById(R.id.idIniciarJuego)
        stopButton = findViewById(R.id.idPresiona)

        startButton.setOnClickListener { _ -> startCount() }
        stopButton.setOnClickListener { _ -> incrementarPuntaje() }
    }

    private fun startCount() {
        n = (0..10).shuffled().first()

        val elNumero = getString(R.string.Numero, Integer.toString(n))
        numeroTextView.text = elNumero

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000

            }

            override fun onFinish() {

            }
        }
        startGame()



    }
    private fun startGame() {
        countDownTimer.start()
        gameStarted = true
    }

    private fun incrementarPuntaje() {
        var tiempoAlReves =0
        tiempoAlReves = 10 -timeLeft

      tiempo = tiempoAlReves-n
        if(tiempo ==0){
        score += 1000
        }
        if (tiempo ==1){
            score += 500
        }
        val puntos = getString(R.string.Puntaje, Integer.toString(score))
        puntajeTextView.text = puntos

    }








}
